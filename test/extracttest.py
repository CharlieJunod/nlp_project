import os
from os import walk
from os import listdir 
from os.path import isfile

def getN(dictionary, n):
    longest_entries = sorted(
        dictionary.items(), key=lambda t: t[1], reverse=True)[:n]
    return [(key, value) for key, value in longest_entries]

def extract_trigrams(dataset_folder, N, encode): 
    files = []
    for (dirpath, dirnames, filenames) in walk(dataset_folder):
        files.extend(filenames)
        break
        
    words = dict()
    
    for file in files:
        f = open(dataset_folder+'/'+file,'r', encoding = encode)
        for line in f.readlines():
            for word in line.split():
                trigram = ""
                word = " "+word+" " #counting the beginnings and endings of words
                for char in word:
                    #making the trigram
                    trigram = trigram + char
                    if(len(trigram)>3):
                        trigram = trigram[1:]
                    else:
                        continue
            
                    #incrementing
                    if trigram in words:
                        words[trigram] += 1
                    else:
                        words[trigram] = 1
            
        f.close()
    if(N<0):
        return words
    
    return getN(words,N)