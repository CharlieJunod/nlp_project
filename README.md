# NLP project
This repository contains the code and the data produced for a project made in the context of the NLP seminar, a course given at the University of Neuchâtel during the Autumn semester of 2018.

It is recommended to read the project's report first, as it explains in detail the data and models contained in this repository.

# Project structure
The files of this project are structured as follows:

 - **report.pdf:** the report of the project
 - **converters:** contains python code used to make a first sorting of the data from the corpus (which is not available in this repository)  
 - **implementation:**  contains the cleaned corpus used for the project and the python code with all the project's implementation  
 - **code**  
	- **exploring_version.ipynb:**  jupyter notebook containing all code that was produced in the early phase of the project  
	- **final_version.ipynb:**  jupyter notebook containing the final version of the project, with the four classifiers presented in the report  
	- **/retrieved features:**  contains files storing features from the training set; this allows not to have to recompute them at every run  
	- **/dataset_test:**  the files kept as test set  
	- **/dataset_test_strat:**  the test set having the same proportion of each language  
	- **/dataset_train:**  the training set  
 - **/outputs:**  contains the result of the models' classification  
 - **/report:**  contains the final report as well as the two presentations

# How to run the project
The project was coded in Python 3.7 using jupyter notebook. The instructions for installing Jupyter are listed here:
http://jupyter.org/install

The project can also be opened using google colaboratory; however, since the implementation relies on local files (the dataset), it will not be possible to run it this way.