\babel@toc {english}{}
\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}State of the Art}{2}
\contentsline {section}{\numberline {3}Methodology}{3}
\contentsline {subsection}{\numberline {3.1}Corpus}{3}
\contentsline {subsection}{\numberline {3.2}Models}{4}
\contentsline {subsubsection}{\numberline {3.2.1}Data representation}{5}
\contentsline {subsubsection}{\numberline {3.2.2}Classification methods}{6}
\contentsline {section}{\numberline {4}Evaluation}{8}
\contentsline {subsection}{\numberline {4.1}KLD}{8}
\contentsline {subsection}{\numberline {4.2}Distance}{9}
\contentsline {section}{\numberline {5}Conclusion}{14}
\contentsline {section}{\numberline {6}References}{15}
\contentsline {section}{\numberline {7}Annex}{16}
