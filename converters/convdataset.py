from uniconv import *

#removes all text that is not headers or article from the datatset
def convdataset(EN=True,PT=False,SP=False):

	EN_balises_lone = ['DOC']
	PT_balises_lone = ['DOC']
	SP_balises_lone = ['DOC']

	EN_balises_content = ['HL','LP','TEXT']
	PT_balises_content = ['TEXT']
	SP_balises_content = ['TITLE','TEXT']


	EN_dir = "EN/1992"
	PT_dir = "PT"
	SP_dir = "SP/efe"

	EN_format = ''
	PT_format = 'sgml'
	SP_format = 'sgml'

	EN_encoding = 'ascii'
	PT_encoding = 'latin_1'
	SP_encoding = 'latin_1'

	indirprefix = "dataset/"
	outdirprefix = "dataset_converted/"

	if(EN):
		convall(indirprefix+EN_dir, EN_format, outdirprefix+EN_dir, EN_balises_lone, EN_balises_content, EN_encoding)
	if(PT):
		convall(indirprefix+PT_dir, PT_format, outdirprefix+PT_dir, PT_balises_lone, PT_balises_content, PT_encoding)
	if(SP):
		convall(indirprefix+SP_dir, SP_format, outdirprefix+SP_dir, SP_balises_lone, SP_balises_content, SP_encoding)